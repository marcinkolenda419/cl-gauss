(in-package :mk-numeric)

(defun solve-upper (U)
  "Rozwiązuje równanie przy macierzy U górnietrójkątnej."
  (let* ((d (array-dimension U 0))
	 (x (make-array (list d) :initial-element 0)))
    (loop for i from (1- d) downto 0
       do (setf (aref x i)
		(/
		 (- (aref U i (1- (array-dimension U 1)))
		    (loop for j from (1+ i) upto (1- d)
		       sum (*
			    (aref U i j)
			    (aref x j))))
		 (aref U i i))))
    x))
;; > (solve-upper #2A((1 2 3 1)
;; 		      (0 5 6 1)
;; 		      (0 0 10 1)))
;; #(27/50 2/25 1/10)

(defun solve-lower (L)
  "Rozwiązuje równanie przy macierzy L dolnotrójkątnej."
  (let* ((d (array-dimension L 0))
	 (x (make-array (list d) :initial-element 0)))
    (loop for i from 0 upto (1- d)
       do (setf (aref x i)
		(/
		 (- (aref L i (1- (array-dimension L 1)))
		    (loop for j from 0 upto (1- i)
		       sum (* (aref L i j)
			      (aref x j))))
		 (aref L i i))))
    x))
;; > (solve-lower #2A ((1 0 0 1)
;; 		       (4 5 0 1)
;; 		       (7 8 10 1)))
;; #(1 -3/5 -3/25)

(defun T-matrix (d n m l)
  "Tworzy macierz T_{N,M} wymiaru D i współczynniku L (por. wykład.)."
  (let ((matrix (make-ident-array d)))
    (setf (aref matrix n m) l)
    matrix))
;; > (T-matrix 3 2 1 5)
;; #2A((1 0 0) (0 1 0) (0 5 1))

(defun transform-matrix-to-upper (x)
  "Returns upper triangular matrix equivalent(in Gauss elimination
method) to matrix X."
  (let ((m (array-dimension x 0))
	(n (array-dimension x 1)))
    (loop for j from 0 upto (- n 2) do
	 (loop for i from (1+ j) upto (1- m) do
	      (progn
		(when (= (aref x j j) 0)
		  (matrix-swap-rows x j
				    (loop for k from (1+ j) upto (- n 2)
				       when (/= (aref x k j) 0)
				       return k))
		  )
		(setf x
		      (multiply-matrix
		       (T-matrix m i j (- (/
					   (aref x i j)
					   (aref x j j))))
		       x))))))
  x)
;; > (transform-matrix-to-upper #2A((1 2 3)
;;    			            (4 5 6)
;; 			            (7 8 10)))
;; #2A((1 2 3) (0 -3 -6) (0 0 1))
