(in-package #:asdf-user)

(defsystem "mk-numeric"
  :author ("Marcin Kolenda")
  :licence "BSD-2-Clause"
  :version "0.0.0"
  :description "Metody numeryczne."
  :long-description "Implementacja kilku metod numerycznych."
  :depends-on ("vgplot")
  :serial t  
  :components ((:file "package")
	       (:module ""
			:depends-on ("package")
			:components
			((:file "gauss")
			 (:file "QR-decomposition")
			 (:file "utils")
			 (:file "interpolation")
			 ))))
