(in-package :mk-numeric)
(declaim (optimize (speed 0) (space 0) (debug 3)))


(defun normalization (vs)
  "Zwraca znormalizowane wektory VS."
  (map (type-of vs) (lambda (v) (scalar-vector-multiplication (/ 1 (norm v)) v)) vs))

(defun qr-decomposition (matrix)
  "Zwraca listę (Q, R) gdzie QR = MATRIX.
Q jest macierzą ortogonalną,
R jest macierzą górnie trójkątną.

O macierzy MATRIX zakładamy, że jest kwadratowa i jej wyznacznik jest niezerowy."
  (let ((ad (array-dimensions matrix)))
    (let ((q (make-array ad :initial-element 0 :element-type 'float))
	  (r (make-array ad :initial-element 0))
	  (bs (make-array (car ad))))
      ;; Oblicz najpierw wszystkie BS
      (setf (aref bs 0) (matrix-column matrix 0))
      (loop for i from 1 upto (1- (car ad))
	 do (let ((a (matrix-column matrix i)))
	      (setf (aref bs i)
		    (reduce #'vector-sum
			    (loop for j from 0 upto (1- i)
			       collect (vector-negation (projection a (aref bs j))))
			    :initial-value a))))
      ;; Znormalizuj BS
      (setf bs (normalization bs))
      ;; Zbuduj macierz Q
      (loop for i from 0 upto (1- (car ad)) do
	   (loop for j from 0 upto (1- (car ad)) do
		(setf (aref q j i)
		      (nth j (aref bs i)))))
      ;; Zbuduj macierz R
      (loop for i from 0 upto (1- (car ad)) do
	   (loop for j from i upto (1- (car ad))
	      do (setf (aref r i j)
		       (scalar-product (matrix-column matrix j) (aref bs i)))))
      (list q r))))
;; > (qr-decomposition #2A ((1 2 3) (4 5 6) (7 8 10)))
;; (#2A((0.12309149 0.90453404 0.40824828)
;;      (0.49236596 0.30151135 -0.81649655)
;;      (0.86164045 -0.30151135 0.40824828))
;;     #2A((8.124039 9.601136 11.939875) (0 0.90453386 1.5075569) (0 0 0.40824842)))
;; > (multiply-matrix (car *) (cadr *))
;; #2A((1.0 1.9999998 3.0000002) (4.0 4.9999995 6.0) (7.0000005 8.0 10.0))
