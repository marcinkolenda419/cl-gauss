(in-package :mk-numeric)

(defun make-ident-array (d)
  "Tworzy macierz identycznościową wymiaru D."
  (let ((M (make-array (list d d) :initial-element 0)))
    (loop for i from 0 upto (1- d)
       do (setf (aref M i i) 1))
    M))
;; > (make-ident-array 3)
;; #2A((1 0 0) (0 1 0) (0 0 1))

(defun multiply-matrix (a b)
  "Mnoży macierz A razy macierz B."
  (let* ((n (array-dimension a 0))
	 (m (array-dimension b 1))
	 (x (make-array (list n m)
			:initial-element 0)))
    (loop
       for i from 0 upto (1- n)
       do (loop
	     for j from 0 upto (1- m)
	     do (setf (aref x i j)
		      (loop for k from 0 to (1- n)
			 summing (* (aref a i k)
				    (aref b k j))))))
    x))
;; > (multiply-matrix #2A((3 0 0) (0 2 0) (0 0 1))
;; 		      #2A((3 0 0) (0 2 0) (0 0 1)))
;; #2A((9 0 0) (0 4 0) (0 0 1))

(defun matrix-swap-rows (matrix i j)
  "Swap I-th row of MATRIX with J-th row of MATRIX in-place. Count from 0."
  (let ((b nil))
    (loop for k from 0 upto (1- (array-dimension matrix 1)) do
	 (setf b (aref matrix i k))
	 (setf (aref matrix i k) (aref matrix j k))
	 (setf (aref matrix j k) b)))
  matrix)
;; > (matrix-swap-rows (make-ident-array 3) 1 2 )
;; #2A((1 0 0) (0 0 1) (0 1 0))

(defun scalar-product (u v)
  "Zwraca iloczyn skalarny wektorów U i V"
  (let ((result 0))
    (mapc (lambda (a b)
	    (incf result (* a b)))
	  u v)
    result))
;; > (scalar-product '(1 2 3) '(3 2 1))
;; 10

(defun vector-negation (u)
  "Zwraca wektor przeciwny do wektora U."
  (mapcar (lambda (a)
	    (- a))
	  u))
;; > (vector-negation '(1 2 3))
;; (-1 -2 -3)

(defun vector-sum (u v)
  "Sumuje wektory U i V wyraz po wyrazie."
  (mapcar (lambda (a b) (+ a b)) u v))


(defun scalar-vector-multiplication (s u)
  "Zwraca wektor powstały po przemnożeniu wektora U przez skalar S."
  (mapcar (lambda (x)
	    (* s x))
	  u))
;; > (scalar-vector-multiplication 3 '(1 2 3))
;; (3 6 9)

(defun matrix-column (matrix n)
  "Zwraca N-tą kolumnę macierzy MATRIX jako wektor. Licz indeks od 0."
  (loop for i from 0 upto (1- (array-dimension matrix 0))
     collect (aref matrix i n)))
;; > (matrix-column (make-ident-array 3) 1)
;; (0 1 0)

(defun matrix-row (matrix n)
  "Zwraca N-ty wiersz macierzy MATRIX jako wektor. Licz indeks od 0."
  (loop for i from 0 upto (1- (array-dimension matrix 0))
     collect (aref matrix i n)))
;; > (matrix-row (make-ident-array 3) 2)
;; (0 0 1)

(defun matrix-transpose (matrix)
  "Zwraca macierz transponowaną do macierzy MATRIX."
  (let* ((dimensions (array-dimensions matrix))
	 (result (make-array (reverse dimensions))))
    (loop for i from 0 upto (1- (car dimensions)) do
	 (loop for j from 0 upto (1- (cadr dimensions)) do
	      (setf (aref result j i) (aref matrix i j))))
    result))
;; > (matrix-transpose #2A((0 1) (0 1) (0 1)))
;; #2A((0 0 0) (1 1 1))

(defun vector-to-row-array (v)
  "Konwertuje wektor V na macierz wierszową."
  (make-array (list 1 (length v)) :initial-contents (list v)))
;; > (vector-to-row-array '(2 4 6))
;; 2A#((2 4 6))

(defun vector-to-column-array (v)
  "Konwertuje wektor V na macierz kolumnową."
  (matrix-transpose (vector-to-row-array v)))
;; > (vector-to-column-array '(5 6 7))
;; #2A((5) (6) (7))

(defun add-column-right (matrix v)
  "Dokleja z prawej strony macierzy MATRIX wektor V jako kolumnę."
  (let* ((height (array-dimension matrix 0))
	 (width (1+ (array-dimension matrix 1)))
	 (m (adjust-array matrix (list height width))))
    (if (null (= height (length v)))
	(error "HEIGHT of MATRIX and length of V are different"))
    (loop for i from 0 to (1- height)
       do (setf (aref m i (1- width)) (nth i v)))
    m))
;; > (add-column-right #2A((1 2 3) (4 5 6) (7 8 9)) '(1 2 3))
;; #2A((1 2 3 1) (4 5 6 2) (7 8 9 3))

(defun add-row-bottom (matrix v)
  "Dokleja z dołu macierzy MATRIX wektor V jako wiersz."
  (let* ((height (1+ (array-dimension matrix 0)))
	 (width (array-dimension matrix 1))
	 (m (adjust-array matrix (list height width)))
	 (vc v))
    (if (null (= width (length v)))
	(error "WIDTH of MATRIX and length of V are different"))
    (loop for i from 0 to (1- width)
       do (setf (aref m (1- height) i) (nth i v)))
    m))
;; > (add-row-bottom #2A((1 2 3) (4 5 6) (7 8 9)) '(1 2 3))
;; #2A((1 2 3) (4 5 6) (7 8 9) (1 2 3))

(defun norm (u)
  "Zwraca normę wektora U."
  (sqrt (scalar-product u u)))
;; > (norm '(1 1 1))
;; 1.7320508

(defun projection (v u)
  "Zwraca projekcję wektora V na wektor U."
  (scalar-vector-multiplication
   (/ (scalar-product v u)
      (scalar-product u u))
   u))
;; > (projection '(1 1 0) '(1 1 1))
;; (2/3 2/3 2/3)

(defun scalar-matrix-multiplication (s m)
  (let ((ret (make-array (array-dimensions m))))
    (loop for i from 0 upto (1- (array-dimension m 0))
       do
	 (loop for j from 0 upto (1- (array-dimension m 1))
	    do
	      (setf (aref ret i j) (* s (aref m i j)))))
    ret))
