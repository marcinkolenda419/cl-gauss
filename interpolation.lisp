(in-package :mk-numeric)

(defun czynniki (xs ys)
  "Zwraca listę list czynników w iloczynach w wielomianie Lagrange'a."
  (let* ((ret nil)
	 (l (length xs)))
    (loop for i from 0 upto (1- l)
	  for y in ys
	  for x2 in xs do 	
	  (let ((rety (list y)))
	    (loop for x in xs
		  for j from 0 upto (1- l) do
		  (if (= i j)
		      1
		    (pushnew `(/ (- a ,x) (- ,x2 ,x)) rety)))
	    (pushnew (reverse rety) ret)
	    ))
    ret))
;; >  (czynniki '(0 1 2) '(6 2 8))
;; ((8 (/ (- A 0) (- 2 0)) (/ (- A 1) (- 2 1)))
;;  (2 (/ (- A 0) (- 1 0)) (/ (- A 2) (- 1 2)))
;;  (6 (/ (- A 1) (- 0 1)) (/ (- A 2) (- 0 2))))

(defun interpoluj (xs ys zs)
  "Wykonuje interpolację dla listy punktów ZS wielomianem Lagrange'a wyznaczonym
przy użyciu danych z list XS i YS."
  (let ((oblczynniki (czynniki xs ys))
	(wyniki nil))
    (loop for z in zs do
	  (push
	   (loop for el in oblczynniki
		 sum
		 (apply #'* (loop for form in (subst z 'a el) collect (eval form))))
	   wyniki))
    (reverse wyniki)))
;; > (interpoluj '(0 1 2) '(6 2 8) '(0 1 2 0.5 1.5 2.5))
;; (6 2 8 2.75 3.75 14.75)

(defun interpoluj-wykres (poczatek koniec res xs ys)
  "Wyświetla okno GNUPlot z wykresem punktowo-liniowym wielomianu interpolacyjnego.
Wykonuje interpolację wielomianem Lagrange'a wyznaczonym przy użyciu
danych z list XS i YS.

POCZATEK oznacza lewy kraniec przedziału w którym wartości wielomianu
interpolacyjnego nas interesują.

KONIEC oznacza prawy kraniec przedziału w którym wartości wielomianu
interpolacyjnego nas interesują.

RES+1 to liczba punktów w jakich wyznaczona zostanie wartość
wielomianu interpolacyjnego."
  (let* ((pts (loop for i from 0 upto res collect (+ poczatek (/ (* i koniec) res))))
	 (zs (interpoluj xs ys pts)))
    (vgplot:plot pts zs "r+;red values;" pts zs)
    (list pts zs)))
;; > (interpoluj-wykres 0 2 10 '(0 1 2) '(6 2 8))
;; ((0 1/5 2/5 3/5 4/5 1 6/5 7/5 8/5 9/5 2) (6 22/5 16/5 12/5 2 2 12/5 16/5 22/5 6 8))




