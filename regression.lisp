(in-package :mk-numeric)

(defun regresja (xs ys)
  (let* ((N (length xs))
	 (Am (make-array '(2 2)))
	 (Bm (make-array '(2 1)))
	 (ret nil))
    (setf (aref Am 0 0) N)
    (setf (aref Am 0 1) (- 0 (loop for x in xs sum x)))
    (setf (aref Am 1 0) (aref Am 0 1))
    (setf (aref Am 1 1) (loop for x in xs sum (* x x)))
    (setf (aref Bm 0 0) (loop for x in xs
			   for y in ys
			   sum (* x y)))
    (setf (aref Bm 1 0) (loop for y in ys sum y))
    (setf ret (scalar-matrix-multiplication
	       (/ 1 (- (* N (aref Am 1 1)) (expt (- 0 (aref Am 0 1)) 2)))
	       (multiply-matrix Am Bm)
	       ))
    (list :a (aref ret 0 0) :b (aref ret 1 0))))
;; > (regresja '(1 2 3) '(5 6 9))
;; (:A 2 :B 8/3)


(defun regresja-wykres (poczatek koniec xs ys)
  (let* ((wsp (regresja xs ys))
	 (pts (list poczatek koniec))
	 (ypts (loop for p in pts
		  collect (+ (*
			      (cadr (member :a wsp))
			      p)
			     (cadr (member :b wsp))))))
    (vgplot:plot xs ys "+r;" pts ypts)))

